<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Exception;
use App\Models\Todo;

class TodoController extends Controller
{
    //insert function
    public function insert(Request $req){
        DB::beginTransaction();

        $valid = Validator::make($req->all(),[
            'title' => 'required|string|unique:todos|max:255'
        ]);

        // to check if the arguments fails
        if($valid->fails()){
            return response()->json([
                'errors' =>  $valid->errors()
            ],400);
        }

        try{
            $todo = Todo::create([
                "title"=>$req->title,
                "description"=>$req->description,
                "status"=>$req->status,
                "created_by"=>$req->created_by,
                "updated_by"=>$req->updated_by
            ]);
            DB::commit();

            return response()->json([
                'message' => "New To Do Activity Added!"
            ]);
        }catch(Exception $e){
            DB::rollback();

            return response()->json([
                'error' => ['Can`t create your entry as of now. Contact the developer to fix it. Error Code : BLOG-0x01 '],
                'msg' =>$e->getMessage()
            ],500);
        }
    }

    //Display
    public function display(){
        if(Todo::count()==0){

            return response()->json([
                'message' => "No record found."
            ]);
        }else{
            return response()->json([
                'data' => Todo::all()
            ]);
        }
    }

    //update
    public function update(Request $req){
        DB::beginTransaction();

        $valid = Validator::make($req->all(),[
            'id' => 'required|integer'
        ]);

        if($valid->fails()){
            return response()->json([
                'errors' => $valid->errors()
            ],400);
        }

        try{
            $update = Todo::where('id',$req->id)->update([
                "title" => $req->title,
                "description" =>$req->description,
                "status"=>$req->status,
                "updated_by" =>$req->updated_by
            ]);

            if($update == 0){
                DB::rollback();

                return response()->json([
                    'message' => 'Article ID does not exists.'
                ],400);
            }else{
                DB::commit();

                return response()->json([
                    'message' =>'Article has been successfully updated!'
                ]);
            }
        }catch(Exception $e){

            DB::rollback();

            return response()->json([
                'errors'    =>  [ 'Can`t create your entry as of now. Contact the developer to fix it. Error Code : BLOG-0x02' ],
                'msg'   =>  $e->getMessage()
            ],500);
        }
    }

    // delete
    public function delete(Request $req){
        DB::beginTransaction();
        $valid = Validator::make($req->all(),[
            'id' => 'required|integer'
        ]);

        if($valid->fails()){
            return response()->json([
                'errors' => $valid->errors()
            ],400);
        }

        try{
            $delete = Todo::findOrFail($req->id);
            $delete->delete();
            DB::commit();

            return response()->json([
                'text' => 'Data has been deleted',
            ]);
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [ 'Can`t create your entry as of now. Contact the developer to fix it. Error Code : BLOG-0x03' ],
                'msg'   =>  $e->getMessage()
            ],500);
        }
    }
// search
    public function search(Request $req){
        DB::beginTransaction();
            try{
    
               $get = DB::table('todos')->where('title', $req->title)->first();
               return $get;
    
            }catch(Exception $e){
    
                return $e->getMessage();
            }
    }
    //filter
    public function filterHour(){
        if(Todo::count()==0){

            return response()->json([
                'message' => "No record found."
            ]);
        }else{
            return response()->json([
                'data' => Todo::all()
            ]);
        }
    }


}
