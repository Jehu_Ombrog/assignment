<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('insert',[TodoController::class,'insert']);
Route::get('display',[TodoController::class,'display']);
Route::post('updated',[TodoController::class,'update']);
Route::post('delete',[TodoController::class,'delete']);
Route::get('search',[TodoController::class,'search']);
